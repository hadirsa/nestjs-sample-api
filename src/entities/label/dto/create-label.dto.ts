export class CreateLabelDto {
    readonly title: string;
    readonly description: string;
    readonly body: string;
    readonly tagList: string[];
}
